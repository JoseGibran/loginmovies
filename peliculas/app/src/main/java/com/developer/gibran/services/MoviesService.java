package com.developer.gibran.services;

import com.developer.gibran.beans.Movie;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by Gibran on 18/07/2015.
 */
public interface MoviesService {

    @GET("/peliculas")
    public void getPelicula(Callback<ArrayList<Movie>> callback);

    @GET("/estrenos")
    public void getEstrenos(Callback<ArrayList<Movie>> callback);

    @GET("/preventas")
    public void getPreVentas(Callback<ArrayList<Movie>> callback);

}
