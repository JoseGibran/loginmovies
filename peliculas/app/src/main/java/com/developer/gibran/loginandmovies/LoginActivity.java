package com.developer.gibran.loginandmovies;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.gibran.beans.User;
import com.developer.gibran.controllers.UserController;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;


public class LoginActivity extends ActionBarActivity {

    private EditText etUser, etPassword;
    private CheckBox chkRemind;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //UserController.addUser(new User("Gibran", "Polonsky", 58198793, "JoseGibran", "123"));
       // UserController.addUser(new User("Jorge", "Monterroso", 4564654, "JorgeMonte", "123"));

        etUser = (EditText)findViewById(R.id.etUser);
        etPassword = (EditText)findViewById(R.id.etPassword);
        chkRemind = (CheckBox)findViewById(R.id.chkRemind);
        UserController.setActivity(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick_btnLogin(View view) {


        ParseUser.logInInBackground(etUser.getText().toString(), etPassword.getText()
                .toString(), new LogInCallback() {

            @Override
            public void done(ParseUser user, ParseException e) {

                if (e != null) {
                    // Show the error message
                    Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    // Start an intent for the dispatch activity
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    Toast.makeText(getApplicationContext(), "Bienvenido: "+ParseUser.getCurrentUser().getString("Name"), Toast.LENGTH_LONG).show();
                    startActivity(intent);
                }
            }
        });



       /* User logedUser = UserController.Logging(etUser.getText().toString(), etPassword.getText().toString());
        if(logedUser!=null){
            Toast.makeText(this, "Bienvenido: " + logedUser.getUserName(), Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, MainActivity.class);

            Bundle extras = intent.getExtras();
            Bundle mBundle = new Bundle();
            mBundle.putInt("userID", logedUser.getIdUser());
            intent.putExtras(mBundle);
            SharedPreferences preferences = getSharedPreferences("Remind", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("IsRemeber", chkRemind.isChecked());
            editor.putString("userName", logedUser.getUserName());
            editor.commit();
            startActivity(intent);
            this.finish();
        }else{
            Toast.makeText(this, "Usuario o Password incorrecto", Toast.LENGTH_SHORT).show();
        }*/
    }
    public void onClick_btnRegister(View view){
        Intent intent = new Intent(this, RegisterActivity.class);

        startActivity(intent);
    }
    public void onClick_btnForgotPassword(View view){
        Intent intent= new Intent (this, ForgetPasswordActivity.class);
        intent.putExtra("userName", etUser.getText().toString());
        startActivity(intent);
    }

}
