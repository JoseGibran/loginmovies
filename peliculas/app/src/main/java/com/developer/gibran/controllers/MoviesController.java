package com.developer.gibran.controllers;

/**
 * Created by Gibran on 30/05/2015.
 */
import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.util.ArrayList;

import com.developer.gibran.beans.Movie;
import com.developer.gibran.helpers.SQLiteManager;

import retrofit.RestAdapter;
;

public class MoviesController {

    private static ArrayList<Movie> movies = new ArrayList<Movie>();
    private static ArrayList<Movie> favorites = new ArrayList<Movie>();

    private static Activity activity;
    private static SQLiteManager sqLiteManager = null;
    private static SQLiteDatabase db =  null;
    private static final String QUERY_MOVIES = "SELECT movieId, name, description, author FROM Movie";
   // private static String QUERY_FAVORITES = "SELECT Movie.movieId, Movie.name, Movie.description, Movie.author FROM Favorite INNER JOIN Movie ON Favorite.movieId = Movie.movieId WHERE Favorite.userId=";
    private static String QUERY_FAVORITES = "SELECT movieId from Favorite WHERE userId = " ;
    private static RestAdapter restAdapter;
    public static void setActivity(Activity act){
        activity = act;
        sqLiteManager = new SQLiteManager(activity, "DBCine", null, 1);
        db = sqLiteManager.getWritableDatabase();
    }

    public static ArrayList<Movie> getMovies(){
        return movies;
    }

    public static void setMovies(ArrayList<Movie> mov){
        movies = mov;
    }

    /*

    public static ArrayList<Movie> getMovies(){
        movies.clear();
        if(db != null){
            Cursor c = db.rawQuery(QUERY_MOVIES, null);
            if(c.moveToFirst()){
                do{
                    Movie movie = new Movie(c.getInt(0), c.getString(1), c.getString(2), c.getString(3));
                    movies.add(movie);

                }while(c.moveToNext());
            }
        }
        return movies;
    }*/

    public static ArrayList<Movie> getFavorites(String userId) {
        favorites.clear();
        if(db != null){
            Cursor c = db.rawQuery(QUERY_FAVORITES + "'" + userId + "';", null);
            if(c.getCount()> 0){
                if(c.moveToFirst()){
                    do{
                        Movie movie = findMovieById(c.getInt(0));
                        favorites.add(movie);

                    }while(c.moveToNext());
                }
            }else{
                Toast.makeText(activity,"No tiene ningun favorito", Toast.LENGTH_LONG).show();
            }

        }
        return favorites;
    }
/*
    public static void addMovie(Movie movie){
        ContentValues newMovie = new ContentValues();
        newMovie.put("name", movie.getName());
        newMovie.put("description", movie.getDescription());
        newMovie.put("author", movie.getAuthor());
        db.insert("Movie", null, newMovie);
    }
    public static void deleteMovie(Movie movie){

        db.delete("User", "idUser=" + movie.getMovieId(), null);
    }

    public Movie findMovieByName(String name){
        for(Movie movie: getMovies()){
            if(movie.getName().equals(name)){
                return movie;
            }
        }
        return null;
    }*/
    public static Movie findMovieById(int idMovie){
        for(Movie movie: getMovies()){
            if(movie.getId()==idMovie){
                return movie;
            }
        }
        return null;
    }
/*
    public static void updateMovie(Movie movie){
        ContentValues updateMovie = new ContentValues();
        updateMovie.put("name", movie.getName());
        updateMovie.put("description", movie.getDescription());
        updateMovie.put("author", movie.getAuthor());
        db.update("Movie", updateMovie, "movieId="+movie.getMovieId(), null );
    }
*/

    public static boolean addFavorite(Movie movie, String userId){
            if(db != null){
                ContentValues newFavorite = new ContentValues();
                newFavorite.put("movieId", movie.getId());
                newFavorite.put("userId", userId);
                db.insert("Favorite", null, newFavorite);
                return true;
            }
        return false;

    }
    public static void deleteFavorite(Movie movie, String userId){
        db.delete("Favorite", "movieId=" + movie.getId() + " AND userId=" + userId, null);
    }
    public Movie findFavorite(String title, String userId){
        for(Movie movie: getFavorites(userId)){
            if(movie.getTitulo().equals(title)){
                return movie;
            }
        }
        return null;
    }
    /*
    private static boolean unique(String name){
        for(Movie movie : getMovies()){
            if(movie.getName().equalsIgnoreCase(name)){
                return false;
            }
        }
        return true;
    }

    public static void closeDB(){
        db.close();
    }*/

}
