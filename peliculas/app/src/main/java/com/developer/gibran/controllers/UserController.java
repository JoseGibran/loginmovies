package com.developer.gibran.controllers;

/**
 * Created by Gibran on 18/05/2015.
 */
import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import com.developer.gibran.beans.User;
import com.developer.gibran.helpers.SQLiteManager;

public class UserController {
    private static ArrayList<User> userList = new ArrayList<User>() ;
    private static Activity activity ;

    private static SQLiteManager sqLiteManager = null;
    private static SQLiteDatabase db =  null;
    private static final String QUERY = "SELECT userId, name, lastName, phone, mail, userName, password FROM User";
    private static User currentUser;

    public static Activity getActivity(){
        return activity;
    }
    public static void setActivity(Activity act){
        activity = act;
        sqLiteManager = new SQLiteManager(activity, "DBCine", null, 1);
        db = sqLiteManager.getWritableDatabase();




    }
    public static ArrayList<User> getUserList(){
        userList.clear();
        if(db != null){
            Cursor c = db.rawQuery(QUERY, null);
            if(c.moveToFirst()){
                do{
                    User user = new User(c.getInt(0), c.getString(1), c.getString(2), c.getInt(3), c.getString(4), c.getString(5), c.getString(6));
                    userList.add(user);
                }while(c.moveToNext());
            }
        }
        return userList;
    }

    public static User Logging(String username, String password){
        for(User user: getUserList()){
            if(user.getUserName().equals(username)){
                if(user.getPassword().equals(password)){
                    setCurrentUser(user);
                    db.close();
                    return user;
                }else{
                    return null;
                }
            }
        }
        return null;
    }
    public static User getUserById(int id){
        for(User user: getUserList()){
            if(user.getIdUser() == id){
                return user;
            }
        }
        return null;
    }
    public static User getUserByUserName(String username){
        for(User user: getUserList()){
            if(user.getUserName().equals(username)){
                return user;
            }
        }
        return null;
    }
    public static boolean unique(String userName){
        for(User user : getUserList()){
            if(user.getUserName().equalsIgnoreCase(userName)){
                return false;
            }
        }
        return true;
    }

    public static int addUser(User user){
        int insertedId= 0;
        if(unique(user.getUserName())){
            ContentValues newUser = new ContentValues();
            newUser.put("name", user.getFirstName());
            newUser.put("lastName", user.getLastName());
            newUser.put("phone", user.getPhone());
            newUser.put("mail", user.getMail());
            newUser.put("userName", user.getUserName());
            newUser.put("password", user.getPassword());
            db.insert("User", null, newUser);
            Cursor c  =db.rawQuery("SELECT userId FROM User WHERE userId = (SELECT MAX(userId)  FROM User);", null);
            if(c.moveToFirst()){
                do{
                    insertedId = c.getInt(0);
                }while(c.moveToNext());
            }
            return insertedId;
        }else{
            return 0;
        }

    }
    public static void removeUser(User user){
        db.delete("User", "userId="+user.getIdUser(), null);
    }
    public static void updateUser(User user){
        ContentValues updateUser = new ContentValues();
        updateUser.put("userId", user.getIdUser());
        updateUser.put("name", user.getFirstName());
        updateUser.put("lastName", user.getLastName());
        updateUser.put("phone", user.getPhone());
        updateUser.put("mail", user.getMail());
        updateUser.put("userName", user.getUserName());
        updateUser.put("password", user.getPassword());
        db.update("User", updateUser, "userId="+user.getIdUser(), null);
    }
    public static void setCurrentUser(User user){
        currentUser = user;
    }
    public static User getCurrentUser(){
        return currentUser;
    }
}
