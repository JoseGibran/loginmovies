package com.developer.gibran.loginandmovies;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import com.developer.gibran.adapters.MoviesAdapter;
import com.developer.gibran.beans.Movie;
import com.developer.gibran.controllers.MoviesController;
import com.developer.gibran.loginandmovies.R;
import com.developer.gibran.services.MoviesService;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private ListView lvEstrenos, lvPreVentas;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        Resources res = getResources();

        TabHost tabs=(TabHost)getView().findViewById(android.R.id.tabhost);
        tabs.setup();

        TabHost.TabSpec spec=tabs.newTabSpec("mitab1");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Estrenos",
                res.getDrawable(android.R.drawable.ic_btn_speak_now));
        tabs.addTab(spec);

        spec=tabs.newTabSpec("mitab2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Cartelera",
                res.getDrawable(android.R.drawable.ic_dialog_map));
        tabs.addTab(spec);

        tabs.setCurrentTab(0);

        lvEstrenos = (ListView)getView().findViewById(R.id.lvEstrenos);
        lvPreVentas = (ListView)getView().findViewById(R.id.lvPreVentas);


        RestAdapter restAdapter;
        restAdapter = new RestAdapter.Builder().setEndpoint("http://192.168.43.182/Cine/public/api").build();

        MoviesService servicio =restAdapter.create(MoviesService.class);

        servicio.getEstrenos(new Callback<ArrayList<Movie>>() {
            @Override
            public void success(ArrayList<Movie> movies, Response response) {

                MoviesAdapter adapterMovies = new MoviesAdapter(getActivity(), movies);

                lvEstrenos.setAdapter(adapterMovies);

            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(getActivity(), retrofitError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        servicio.getPreVentas(new Callback<ArrayList<Movie>>() {
            @Override
            public void success(ArrayList<Movie> movies, Response response) {
                MoviesAdapter adapter = new MoviesAdapter(getActivity(), movies);

                lvPreVentas.setAdapter(adapter);

            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(getActivity(), retrofitError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

}
