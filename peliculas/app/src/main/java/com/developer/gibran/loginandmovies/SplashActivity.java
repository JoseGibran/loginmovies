package com.developer.gibran.loginandmovies;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.developer.gibran.controllers.UserController;
import com.developer.gibran.loginandmovies.R;
import com.parse.ParseUser;

public class SplashActivity extends ActionBarActivity {

    private static final int SPLASH_DISPLAY = 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = null;
               /* SharedPreferences pref = getApplicationContext().getSharedPreferences("Remind", Context.MODE_PRIVATE);

                Boolean isRemembered = pref.getBoolean("IsRemeber", false);
                if(isRemembered){
                    UserController.setActivity(SplashActivity.this);
                    UserController.setCurrentUser(UserController.getUserByUserName(pref.getString("userName", null)));
                    if(UserController.getCurrentUser()!= null){

                        intent = new Intent(getApplicationContext(), MainActivity.class);
                        Toast.makeText(getApplicationContext(), " Bienvenido: "+UserController.getCurrentUser().getFirstName(), Toast.LENGTH_LONG).show();
                    }else{
                        intent = new Intent(getApplicationContext(), LoginActivity.class);
                    }
                }else{
                    intent = new Intent(getApplicationContext(), LoginActivity.class);
                }
                startActivity(intent);
                finish();*/


                if(ParseUser.getCurrentUser() == null){
                    intent = new Intent(getApplicationContext(), LoginActivity.class);

                }else{
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    Toast.makeText(getApplicationContext(), "Bienvenido: "+ParseUser.getCurrentUser().getString("Name"), Toast.LENGTH_LONG).show();
                }
                startActivity(intent);
                finish();
            }
        }, SPLASH_DISPLAY);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
