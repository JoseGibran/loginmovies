package com.developer.gibran.adapters;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.developer.gibran.beans.Movie;

import com.developer.gibran.loginandmovies.R;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.ImageLoader;

import static com.nostra13.universalimageloader.core.DisplayImageOptions.*;



/**
 * Created by Gibran on 30/05/2015.
 */
public class MoviesAdapter extends ArrayAdapter<Movie> {
    public ArrayList<Movie>movies;
    private Context context;
    public MoviesAdapter(Context context, ArrayList<Movie> datos){
        super(context, R.layout.listitem_movie, datos);
        this.movies = datos;
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.listitem_movie, null);

        TextView tvNombrePelicula = (TextView)item.findViewById(R.id.tvNombrePelicula);
        TextView tvGenero = (TextView)item.findViewById(R.id.tvGenero);
        TextView tvRated = (TextView)item.findViewById(R.id.tvRated);

        ImageView ivPortada = (ImageView)item.findViewById(R.id.ivPortada);

        DisplayImageOptions defaultOptions = new Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                context)
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);

        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true).build();


        imageLoader.displayImage(movies.get(position).getImage(), ivPortada, options);
        tvGenero.setText( movies.get(position).getGenero());
        tvNombrePelicula.setText( movies.get(position).getTitulo());
        tvRated.setText(movies.get(position).getRated());

        return item;
    }



}
