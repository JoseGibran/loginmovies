package com.developer.gibran.loginandmovies;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.developer.gibran.adapters.MoviesAdapter;
import com.developer.gibran.beans.Movie;
import com.developer.gibran.controllers.MoviesController;
import com.developer.gibran.controllers.UserController;
import com.developer.gibran.loginandmovies.R;
import com.developer.gibran.services.MoviesService;
import com.parse.ParseUser;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoviesFragment extends Fragment {


    private ListView lvMovies;

    public MoviesFragment() {

        // Required empty public constructor
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_movies, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        lvMovies = (ListView)getView().findViewById(R.id.lvMovies);


        RestAdapter restAdapter;
        restAdapter = new RestAdapter.Builder().setEndpoint("http://192.168.43.182/Cine/public/api").build();

        MoviesService servicio =restAdapter.create(MoviesService.class);

        servicio.getPelicula(new Callback<ArrayList<Movie>>() {
            @Override
            public void success(ArrayList<Movie> mov, Response response) {
                MoviesController.setMovies(mov);
                MoviesAdapter adapterMovies = new MoviesAdapter(getActivity(), MoviesController.getMovies());

                lvMovies.setAdapter(adapterMovies);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(getActivity(), retrofitError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


        registerForContextMenu(lvMovies);
        //userId = UserController.getCurrentUser().getIdUser();
        lvMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                Bundle extras = new Bundle();

                extras.putString("movieName", MoviesController.getMovies().get(position).getTitulo());
                extras.putString("rated",MoviesController.getMovies().get(position).getRated() );
                extras.putString("sinopsis",MoviesController.getMovies().get(position).getSinopsis() );
                intent.putExtras(extras);
                getActivity().startActivity(intent);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();

        if(v.getId() == R.id.lvMovies){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            Movie movie = (Movie) lvMovies.getAdapter().getItem(info.position);
            menu.setHeaderTitle(movie.getTitulo());
            inflater.inflate(R.menu.menu_ctx_lista_pelicula, menu);
        }
    }
    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch(item.getItemId()){
            case R.id.ctxAgregarFavorito:
                if(MoviesController.addFavorite(MoviesController.getMovies().get(info.position), ParseUser.getCurrentUser().getObjectId())){
                    Toast.makeText(getActivity(), "Pelicula["+info.position+"] Agregada A Favoritos", Toast.LENGTH_LONG).show();
                    return true;
                }
            default:
                return super.onContextItemSelected(item);
        }
    }
}
