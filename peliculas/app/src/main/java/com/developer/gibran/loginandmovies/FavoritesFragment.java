package com.developer.gibran.loginandmovies;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.developer.gibran.adapters.MoviesAdapter;
import com.developer.gibran.beans.Movie;
import com.developer.gibran.controllers.MoviesController;
import com.developer.gibran.controllers.UserController;
import com.developer.gibran.loginandmovies.R;
import com.parse.ParseUser;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritesFragment extends Fragment {

    private ListView lvFavorites;

    public FavoritesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorites, container, false);
    }
    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        lvFavorites = (ListView)getView().findViewById(R.id.lvFavorites);

        MoviesController.setActivity(getActivity());
        MoviesAdapter adapterMovies = new MoviesAdapter(getActivity(),  MoviesController.getFavorites(ParseUser.getCurrentUser().getObjectId()));
       // MoviesController.closeDB();
        lvFavorites.setAdapter(adapterMovies);
        registerForContextMenu(lvFavorites);
    }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        if(v.getId() == R.id.lvFavorites){
            AdapterView.AdapterContextMenuInfo info  = (AdapterView.AdapterContextMenuInfo)menuInfo;
            Movie movie = (Movie)lvFavorites.getAdapter().getItem(info.position);
            menu.setHeaderTitle(movie.getTitulo());
            inflater.inflate(R.menu.menu_ctx_list_favorites, menu);
        }
    }
    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch(item.getItemId()){
            case R.id.ctxDeleteFavorite :
                    MoviesController.deleteFavorite(MoviesController.getFavorites(ParseUser.getCurrentUser().getObjectId()).get(info.position), ParseUser.getCurrentUser().getObjectId());
                    Toast.makeText(getActivity(), "Pelicula eliminada de Favoritos", Toast.LENGTH_LONG).show();
                    MoviesAdapter adapterMovies = new MoviesAdapter(getActivity(),  MoviesController.getFavorites(ParseUser.getCurrentUser().getObjectId()));
                    lvFavorites.setAdapter(adapterMovies);
                    return true;
            default :
                return super.onContextItemSelected(item);
        }
    }
}
