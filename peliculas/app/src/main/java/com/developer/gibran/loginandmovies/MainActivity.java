package com.developer.gibran.loginandmovies;


import android.support.v4.app.FragmentTransaction;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.support.v7.widget.Toolbar;
import com.developer.gibran.controllers.UserController;
import com.parse.ParseUser;


public class MainActivity extends ActionBarActivity implements NavigationDrawerFragment.FragmentDrawerListener   {

    public MainActivity(){
        super();
    }

    private Toolbar mToolbar;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment)getSupportFragmentManager().findFragmentById(R.id.navigation_drawer_fragment);
        drawerFragment.setUp((DrawerLayout) findViewById(R.id.drawer_layout), mToolbar, R.id.navigation_drawer_fragment);
        drawerFragment.setDrawerListener(this);
        fragmentManager = getSupportFragmentManager();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent = null;
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }else if(id == R.id.action_logout){
            intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {

        Fragment selectedFragment = null;
        String title = getString(R.string.app_name);

        switch(position){
            case 1:
                selectedFragment = new HomeFragment();
                title = "Home";
                break;
            case 2:
                selectedFragment = new ProfileFragment();
                ParseUser.getCurrentUser().getUsername();
                break;
            case 3:
                selectedFragment =new MoviesFragment();
                title = "Movies";
                break;
            case 4:
                selectedFragment = new FavoritesFragment();
                title = "Favorites";
                break;
            case 5:
                selectedFragment = new EnventsFragment();
                title = "Events";
                break;
            case 6:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            case 7:
                Intent intent2 = new Intent(this, LoginActivity.class);
                //UserController.setCurrentUser(null);
                ParseUser.logOut();
                startActivity(intent2);
                this.finish();
                break;
        }
        if(selectedFragment != null){
            changeFragment(selectedFragment, title);
        }
    }
    public  void changeFragment(Fragment fragment, String title){
        ((RelativeLayout)findViewById(R.id.ContainerLayout)).removeAllViewsInLayout();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.ContainerLayout, fragment);
        fragmentTransaction.commit();
        getSupportActionBar().setTitle(title);
    }
}
