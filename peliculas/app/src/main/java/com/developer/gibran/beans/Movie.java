package com.developer.gibran.beans;

/**
 * Created by informatica on 22/05/2015.
 */
public class Movie {
    private String titulo, sinopsis, trailer_url, image, rated, genero;
    private int id;

    public Movie(){

    }

    public Movie(String titulo, String sinopsis, String trailer_url, String image, String rated, String genero, int id) {
        this.titulo = titulo;
        this.sinopsis = sinopsis;
        this.trailer_url = trailer_url;
        this.image = image;
        this.rated = rated;
        this.genero = genero;
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getTrailer_url() {
        return trailer_url;
    }

    public void setTrailer_url(String trailer_url) {
        this.trailer_url = trailer_url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRated() {
        return rated;
    }

    public void setRated(String rated) {
        this.rated = rated;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}