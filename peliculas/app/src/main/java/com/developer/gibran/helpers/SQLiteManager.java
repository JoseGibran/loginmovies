package com.developer.gibran.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Gibran on 04/06/2015.
 */


public class SQLiteManager extends SQLiteOpenHelper {

  //  private String tableUser=  "CREATE TABLE User( userId INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(255), lastName VARCHAR(255), phone INTEGER, mail VARCHAR(255),  userName VARCHAR(255),  password VARCHAR(255));" ;
   // private String tableMovie = "CREATE TABLE Movie( movieId INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(255), description VARCHAR(255), author VARCHAR(255));";
    private String tableFavorite = "CREATE TABLE Favorite( movieId INTEGER NOT NULL, userId VARCHAR(255) NOT NULL, PRIMARY KEY(movieId, userId));";

    public SQLiteManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
      public void onCreate(SQLiteDatabase db) {
       // db.execSQL(tableUser);
       // db.execSQL(tableMovie);
        db.execSQL(tableFavorite);
       // db.execSQL("INSERT INTO User(name, lastname, phone, mail, userName, password) VALUES ('Jose Gibran', 'Polonsky', 58198793, 'josegibranpolonsky@outlook.es', 'JoseGibran', '123');");
      /*  db.execSQL("INSERT INTO Movie(name, description, author) VALUES('Avengers','Pelicula de Accion','no lo se :D');");
        db.execSQL("INSERT INTO Movie(name, description, author) VALUES('Ted 2','Pelicula de Drama','no lo se :D');");
        db.execSQL("INSERT INTO Movie(name, description, author) VALUES('Divergente','Pelicula de Accion','no lo se :D');");
        db.execSQL("INSERT INTO Movie(name, description, author) VALUES('Mase Runner','Pelicula de Accion','no lo se :D');");
        db.execSQL("INSERT INTO Movie(name, description, author) VALUES('Juegos del hambre','Pelicula de Accion','no lo se :D');");
        db.execSQL("INSERT INTO Movie(name, description, author) VALUES('Bajo la misma estrella','Pelicula de Accion','no lo se :D');");
        db.execSQL("INSERT INTO Movie(name, description, author) VALUES('Batman','Pelicula de Accion','no lo se :D');");
        db.execSQL("INSERT INTO Movie(name, description, author) VALUES('IronMan','Pelicula de Accion','no lo se :D');");
        db.execSQL("INSERT INTO Movie(name, description, author) VALUES('Carry Me Home','Pelicula de Accion','no lo se :D');");
        db.execSQL("INSERT INTO Movie(name, description, author) VALUES('The lord of rings','Pelicula de Ciencia Ficcion','no lo se :D');");
        db.execSQL("INSERT INTO Movie(name, description, author) VALUES('Starwars','Pelicula de Ciencia Ficcion','no lo se :D');");*/

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


    }
}
