package com.developer.gibran.loginandmovies;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.developer.gibran.adapters.NavigationDrawerAdapter;
import com.developer.gibran.beans.User;
import com.developer.gibran.controllers.UserController;
import com.developer.gibran.helpers.RecyclerItemClickListener;
import com.parse.ParseUser;


/**
 * A simple {@link Fragment} subclass.
 */
public class    NavigationDrawerFragment extends Fragment {

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private FragmentDrawerListener mDrawerListener;

    private View containerView;

    private int ICONS[] = {R.drawable.ic_home, R.drawable.ic_user, R.drawable.ic_movies, R.drawable.ic_favorites,R.drawable.ic_calendarios, R.drawable.ic_settings, R.drawable.ic_logout};
    private String TITLES[] = {"Home", "Profile", "Movies", "Favorites","Events", "Settings", "logout"};
    private String NAME = "UnLoged";
    private String EMAIL = "UnLoged";
    private int PROFILE = R.drawable.default_profile;
    private RecyclerView recyclerView;


    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle state){
        super.onActivityCreated(state);


        mAdapter = new NavigationDrawerAdapter(TITLES, ICONS, ParseUser.getCurrentUser().getString("Name") + " " + ParseUser.getCurrentUser().getString("LastName"), ParseUser.getCurrentUser().getEmail(), PROFILE);


        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mDrawerListener.onDrawerItemSelected(view, position);
                mDrawerLayout.closeDrawer(containerView);
            }
        }));
        recyclerView = (RecyclerView)getView().findViewById(R.id.RecyclerView);
        int alpha = (int)(0.75 * 255.0f);
        recyclerView.setBackgroundColor(Color.argb(alpha, 244, 244, 244));
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.RecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);



        return v;
    }



    public void setUp(DrawerLayout drawerLaout, Toolbar toolbar, int fragmentId) {
        this.mDrawerLayout = drawerLaout;
        this.mToolbar = toolbar;
        this.containerView = getActivity().findViewById(fragmentId);

        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLaout, toolbar, R.string.drawer_open, R.string.drawer_close){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                mToolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    public void setDrawerListener(FragmentDrawerListener drawerListener){
        this.mDrawerListener = drawerListener;
    }
    public interface FragmentDrawerListener{
        public void onDrawerItemSelected(View view, int position);
    }
}
