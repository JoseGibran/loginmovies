package com.developer.gibran.loginandmovies;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.developer.gibran.beans.User;
import com.developer.gibran.controllers.UserController;
import com.developer.gibran.loginandmovies.R;

public class ForgetPasswordActivity extends ActionBarActivity {

    private EditText etUserName, etPassword, etPassword2;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserController.setActivity(this);
        user = UserController.getUserByUserName(getIntent().getExtras().getString("userName"));
        setContentView(R.layout.activity_forget_password);
        etUserName = (EditText)findViewById(R.id.forgetUserName);
        etPassword = (EditText)findViewById(R.id.forgetPassword);
        etPassword2 = (EditText)findViewById(R.id.forgetPassword2);
        etUserName.setText(user.getUserName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_forget_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void onClick_btnSave(View view){
        if(etPassword.getText().toString().equals(etPassword2.getText().toString())){
            user.setUserName(etUserName.getText().toString());
            user.setPassword(etPassword.getText().toString());
            UserController.updateUser(user);
            Toast.makeText(this, "Sucessfull Change password", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }else{
            Toast.makeText(this, "The passwords are not equals", Toast.LENGTH_LONG).show();
        }
    }
}
