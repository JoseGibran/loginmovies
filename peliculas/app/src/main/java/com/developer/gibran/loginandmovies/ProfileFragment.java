package com.developer.gibran.loginandmovies;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.developer.gibran.controllers.UserController;
import com.developer.gibran.loginandmovies.R;
import com.parse.ParseUser;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private TextView tvName, tvMail, tvPhone;
    private Button btnEdit;


    public ProfileFragment(){
        super();
    }

    @Override
    public void onActivityCreated(Bundle state){
        super.onActivityCreated(state);
        tvName = (TextView)getView().findViewById(R.id.tvNameProfileFragment);
        tvMail = (TextView)getView().findViewById(R.id.tvMailProfileFragment);
        tvPhone = (TextView)getView().findViewById(R.id.textView2);
        tvName.setText(ParseUser.getCurrentUser().getString("Name") + " " + ParseUser.getCurrentUser().getString("LastName"));
        tvMail.setText(UserController.getCurrentUser().getMail());
        tvPhone.setText(String.valueOf(UserController.getCurrentUser().getPhone()));
        btnEdit =(Button)getView().findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                getActivity().startActivity(intent);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

}
