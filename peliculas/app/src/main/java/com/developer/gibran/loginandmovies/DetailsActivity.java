package com.developer.gibran.loginandmovies;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.developer.gibran.loginandmovies.R;

public class DetailsActivity extends ActionBarActivity {

    private TextView tvMovieName, tvGenre, tvAuthor;
    private Toolbar mToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        tvMovieName = (TextView)findViewById(R.id.tvMovieName);
        tvGenre = (TextView)findViewById(R.id.tvGenre);
        tvAuthor = (TextView)findViewById(R.id.tvAuthor);
        tvMovieName.setText( getIntent().getExtras().getString("movieName"));
        tvGenre.setText( getIntent().getExtras().getString("rated"));
        tvAuthor.setText( getIntent().getExtras().getString("sinopsis"));

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
