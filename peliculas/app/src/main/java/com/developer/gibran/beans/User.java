package com.developer.gibran.beans;

/**
 * Created by Gibran on 18/05/2015.
 */
public class User {
    private String firstName, lastName, userName, password, mail;
    private int idUser, phone;

    public User(){

    }
    public User(String firstName, String lastName, int phone, String mail, String userName, String password){
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.userName = userName;
        this.password = password;
        this.mail = mail;
    }

    public User(int idUser, String firstName, String lastName, int phone, String mail, String userName, String password){
       this.idUser = idUser;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.userName = userName;
        this.password = password;
        this.mail = mail;
    }

    public int getIdUser(){
        return this.idUser;
    }
    public void setIdUser(int idUser){
        this.idUser = idUser;
    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getMail(){return mail;}

    public void setMail(String mail){this.mail = mail;}
}
