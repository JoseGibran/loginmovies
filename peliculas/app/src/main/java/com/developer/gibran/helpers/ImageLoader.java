package com.developer.gibran.helpers;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.developer.gibran.loginandmovies.MainActivity;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by gibran on 24/07/15.
 */
public class ImageLoader extends AsyncTask<String, Void, Bitmap> {

    ProgressDialog pDialog;
    MainActivity mainActivity;
    ImageView imageView;
    public ImageLoader(ImageView imageView, MainActivity main){
        this.imageView = imageView;
        this.mainActivity = main;
    }

    @Override
    protected  void onPreExecute(){
        super.onPreExecute();

        pDialog = new ProgressDialog(mainActivity);
        pDialog.setMessage("Cargando Imagen");
        pDialog.setCancelable(true);
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.show();
    }



    @Override
    protected Bitmap doInBackground(String... params) {
        Log.i("doInBackground", "Entra en doInBackground");
        String url= params[0];
        Bitmap image = descargarImagen(url);
        return image;
    }

    @Override
    protected void onPostExecute(Bitmap result){
        super.onPostExecute(result);

    }

    private Bitmap descargarImagen(String imageHttpAddress){
        URL imageUrl = null;
        Bitmap imagen = null;
        try{
            imageUrl = new URL(imageHttpAddress);
            HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
            imagen = BitmapFactory.decodeStream(conn.getInputStream());
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
        return imagen;
    }
}
