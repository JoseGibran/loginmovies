package com.developer.gibran.loginandmovies;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.developer.gibran.beans.User;
import com.developer.gibran.controllers.UserController;
import com.developer.gibran.loginandmovies.R;

public class EditProfileActivity extends ActionBarActivity {

    private EditText etFirstName, etLastName, etPhone, etMail;
    private static MainActivity main;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        etFirstName = (EditText)findViewById(R.id.etFirstName);
        etLastName = (EditText)findViewById(R.id.etLastName);
        etPhone = (EditText)findViewById(R.id.etPhone);
        etMail = (EditText)findViewById(R.id.etMail);



        etFirstName.setText(UserController.getCurrentUser().getFirstName());
        etLastName.setText(UserController.getCurrentUser().getLastName());
        etPhone.setText(String.valueOf(UserController.getCurrentUser().getPhone()));
        etMail.setText(UserController.getCurrentUser().getMail());
        UserController.setActivity(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick_btn(View view){
        Intent intent = null;
        if(view.getId() == R.id.btnCancel){
            intent = new Intent(this, MainActivity.class );
        }else if(view.getId() == R.id.btnDone){
            User user =  UserController.getCurrentUser();
            user.setFirstName(etFirstName.getText().toString());
            user.setLastName(etLastName.getText().toString());
            user.setPhone(Integer.parseInt(etPhone.getText().toString()));
            user.setMail(etMail.getText().toString());
            UserController.updateUser(user);


            intent = new Intent(this, MainActivity.class);
        }
        if(intent != null){
            startActivity(intent);
        }
    }
}
